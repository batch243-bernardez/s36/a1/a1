const Task = require("../models/task");

module.exports.getAllTasks = () => {
		return Task.find({}).then(result => {
			return result
		})
	}

// 2. Create a controller function for retrieving a specific task.
	module.exports.getSpecificTask= () => {
 			return Task.find({}).then(result => {
 				return result
 			})
 		}

 	module.exports.createTask = (requestBody) => {
		let newTask = new Task({
			name: requestBody.name
		})

		return newTask.save().then((task,error) => {
				if(error){
					console.log(error);
					return false;
				}
				else{
					return task;
				}
			})
		}

// 6. Create a controller function for changing the status of a task to "complete".
	module.exports.changeStatus = (taskId,newContent) => {
		return Task.findById(taskId).then((result,error) => {
			if(error){
				console.log(error);
				return false
			}

			result.status = "complete"
			return result.save().then((updatedStatus, saveErr) => {
				if(saveErr){
					console.log(saveErr);
					return false
				}
				else{
					return updatedStatus;
				}

			})
		})
	}
