const express = require("express")

const router = express.Router();

const taskController = require("../controllers/taskController");


// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
	
	router.get("/",(req,res)=>{
		taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
	})


	router.get("/:id", (req,res) => {
		taskController.getSpecificTask().then(resultFromController => res.send(resultFromController));
	})


// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

	router.post("/",(req,res)=>{
		taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	})
	
	router.put("/:id/complete", (req,res) => {
		taskController.changeStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
	})

module.exports = router;