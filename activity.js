const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

const taskRoute = require("./routes/taskRoute");

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/tasks", taskRoute);


mongoose.connect("mongodb+srv://mpyb:mpyb@zuittbatch243.kotcmj5.mongodb.net/B243-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

app.listen(port, () => console.log(`Server running at ${port}`))
